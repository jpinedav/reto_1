package co.com.proyectobase.screenplay.tasks;

import java.util.List;
import co.com.proyectobase.screenplay.interactions.ProcesoInteractivo;
import co.com.proyectobase.screenplay.interactions.SeleccionGenero;
import co.com.proyectobase.screenplay.model.InformacionUsuario;
import co.com.proyectobase.screenplay.userinterface.LaWebAutomationDemoSitePageObject;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.Actor;

public class Registrar implements Task {
	
	LaWebAutomationDemoSitePageObject laWebAutomationDemoSitePageObject;
	
	private List<InformacionUsuario > data;

	public Registrar(List<InformacionUsuario> data) {
		this.data = data;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue(data.get(0).getFirstName()).into(LaWebAutomationDemoSitePageObject.FIRST_NAME));
		actor.attemptsTo(Enter.theValue(data.get(0).getLastName()).into(LaWebAutomationDemoSitePageObject.LAST_NAME));
		actor.attemptsTo(Enter.theValue(data.get(0).getAddress()).into(LaWebAutomationDemoSitePageObject.ADDRESS));
		actor.attemptsTo(Enter.theValue(data.get(0).getEmailAddress()).into(LaWebAutomationDemoSitePageObject.EMAIL_ADDRESS));
		actor.attemptsTo(Enter.theValue(data.get(0).getPhone()).into(LaWebAutomationDemoSitePageObject.PHONE));
		actor.attemptsTo(SeleccionGenero.optGenero(data.get(0).getGender(),LaWebAutomationDemoSitePageObject.GENDER));
		
		if (data.get(0).getHobbies1().equals("Cricket"))
		{
			actor.attemptsTo(Click.on(LaWebAutomationDemoSitePageObject.HOBBIES1));
		}

		if (data.get(0).getHobbies2().equals("Movies"))
		{
			actor.attemptsTo(Click.on(LaWebAutomationDemoSitePageObject.HOBBIES2));
		}
		
		if (data.get(0).getHobbies3().equals("Hockey"))
		{
			actor.attemptsTo(Click.on(LaWebAutomationDemoSitePageObject.HOBBIES3));
		}
		
		actor.attemptsTo(Click.on(LaWebAutomationDemoSitePageObject.LANGUAGES));
		
		actor.attemptsTo(ProcesoInteractivo.BuscarLista(data.get(0).getLanguages1(),LaWebAutomationDemoSitePageObject.LANGUAGES1));
		actor.attemptsTo(ProcesoInteractivo.BuscarLista(data.get(0).getLenguages2(),LaWebAutomationDemoSitePageObject.LANGUAGES1));
		actor.attemptsTo(ProcesoInteractivo.BuscarLista(data.get(0).getLanguages3(),LaWebAutomationDemoSitePageObject.LANGUAGES1));
		
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getSkills()).from(LaWebAutomationDemoSitePageObject.SKILLS));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getCountry()).from(LaWebAutomationDemoSitePageObject.COUNTRY));
		actor.attemptsTo(Click.on(LaWebAutomationDemoSitePageObject.SELECT_COUNTRY));
		actor.attemptsTo(Enter.theValue(data.get(0).getSelectCountry() + "\n").into(LaWebAutomationDemoSitePageObject.SELECT_COUNTRY1));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getYear()).from(LaWebAutomationDemoSitePageObject.DATA_OF_BIRTH_YEAR));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getMonth()).from(LaWebAutomationDemoSitePageObject.DATA_OF_BIRTH_MONTH));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getDay()).from(LaWebAutomationDemoSitePageObject.DATA_OF_BIRTH_DAY));
		actor.attemptsTo(Enter.theValue(data.get(0).getPassword()).into(LaWebAutomationDemoSitePageObject.PASSWORD));
		actor.attemptsTo(Enter.theValue(data.get(0).getConfirmPassword()).into(LaWebAutomationDemoSitePageObject.CONFIRM_PASSWORD));
		actor.attemptsTo(Click.on(LaWebAutomationDemoSitePageObject.BONTON_SUBMIT));
	}

	public static Registrar Usuario(List<InformacionUsuario> dato) {		
		return Tasks.instrumented(Registrar.class, dato);
	}
}
