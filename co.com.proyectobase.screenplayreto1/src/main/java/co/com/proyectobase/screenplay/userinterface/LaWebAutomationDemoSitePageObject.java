package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class LaWebAutomationDemoSitePageObject 
{		
	public static final Target FIRST_NAME = Target.the("Campo Primer Nombre").located(By.xpath("//*[@placeholder='First Name']"));
	public static final Target LAST_NAME = Target.the("Campo Apellido").located(By.xpath("//*[@placeholder='Last Name']"));
	public static final Target ADDRESS = Target.the("Campo Direccion").located(By.xpath("//*[@ng-model='Adress']"));
	public static final Target EMAIL_ADDRESS = Target.the("Campo direccion de correo").located(By.xpath("//*[@ng-model='EmailAdress']"));
	public static final Target PHONE = Target.the("Campo Telefono").located(By.xpath("//*[@ng-model='Phone']"));
	public static final Target GENDER = Target.the("Campo Genero").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[5]/div"));
	public static final Target GENDERM = Target.the("Campo Genero").located(By.xpath("//*[@value='Male']"));
	public static final Target GENDERF = Target.the("Campo Genero").located(By.xpath("//*[@value='FeMale']"));
	public static final Target HOBBIES1 = Target.the("Campo Aficiones").located(By.id("checkbox1"));
	public static final Target HOBBIES2 = Target.the("Campo Aficiones").located(By.id("checkbox2"));
	public static final Target HOBBIES3 = Target.the("Campo Aficiones").located(By.id("checkbox3"));
	public static final Target LANGUAGES = Target.the("Campo Lenguaje").located(By.id("msdd"));
	public static final Target LANGUAGES1 = Target.the("Campo Lenguaje").located(By.id("basicBootstrapForm"));
	public static final Target SKILLS = Target.the("Campo Habilidades").located(By.id("Skills"));
	public static final Target COUNTRY = Target.the("Campo Pais").located(By.id("countries"));
	public static final Target SELECT_COUNTRY = Target.the("Campo Continente").located(By.xpath("//*[@role='combobox']"));
	public static final Target SELECT_COUNTRY1 = Target.the("Campo Continente").located(By.xpath("//*[@type='search']"));
	public static final Target DATA_OF_BIRTH_YEAR = Target.the("Campo A�o Cumplea�os").located(By.id("yearbox"));
	public static final Target DATA_OF_BIRTH_MONTH = Target.the("Campo Mes Cumplea�os").located(By.xpath("//*[@placeholder='Month']"));
	public static final Target DATA_OF_BIRTH_DAY = Target.the("Campo Dia Cumplea�os").located(By.id("daybox"));
	public static final Target PASSWORD = Target.the("Campo Contrase�a").located(By.xpath("//*[@ng-model='Password']"));
	public static final Target CONFIRM_PASSWORD = Target.the("Campo Confirmar Contrase�a").located(By.xpath("//*[@ng-model='CPassword']"));
	public static final Target BONTON_SUBMIT = Target.the("Boton Submit").located(By.id("submitbtn"));
	public static final Target LABEL_PALABRA = Target.the("Label").located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
}
