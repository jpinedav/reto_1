package co.com.proyectobase.screenplay.interactions;

import java.util.List;

import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;

public class SeleccionGenero implements Interaction{
	
	private String dato;
	private Target datoTarget;
	
	public SeleccionGenero(String dato, Target datoTarget) {
		this.dato = dato;
		this.datoTarget = datoTarget;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		List<WebElement> optGenero = datoTarget.resolveFor(actor).findElements(By.tagName("label"));
		
		for( int i=0; i<optGenero.size();i++) {
			if(optGenero.get(i).getText().contains(dato)) {
				optGenero.get(i).click();
				break;
			}
		}	
	}	

	public static SeleccionGenero optGenero (String dato, Target datoTarget) 
	{
		return new SeleccionGenero(dato, datoTarget);
	}


}
