package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.LaWebAutomationDemoSitePageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Validar implements Question<String> {
	
	public static Validar IngresoSatisfactorio () {
		return new Validar();
	}

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(LaWebAutomationDemoSitePageObject.LABEL_PALABRA).viewedBy(actor).asString();
	}
}
