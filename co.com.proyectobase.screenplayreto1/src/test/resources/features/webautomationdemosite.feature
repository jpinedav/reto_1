#Author:jpinedav@choucairtesting.com

@Regresin
Feature: El usuario desea hacer el registro en la pagina Automation Demo Site y validar su ingreso

  @CasoExitoso
  Scenario Outline: Realizar el registro y validar su ingreso
    Given que Carlos quiere acceder a la pagina de Registro
    When el realiza el registro en la pagina
    |firstName|lastName|address|emailAddress|phone|gender|hobbies1|hobbies2|hobbies3|languages1|lenguages2|languages3|skills|country|selectCountry|year|month|day|password|confirmPassword|
 		|<firstName>|<lastName>|<address>|<emailAddress>|<phone>|<gender>|<hobbies1>|<hobbies2>|<hobbies3>|<languages1>|<lenguages2>|<languages3>|<skills>|<country>|<selectCountry>|<year>|<month>|<day>|<password>|<confirmPassword>|
    Then el verifica que se carga la pantalla con texto - Double Click on Edit Icon to EDIT the Table Row.
    
    Examples:
    |firstName|lastName|address|emailAddress|phone|gender|hobbies1|hobbies2|hobbies3|languages1|lenguages2|languages3|skills|country|selectCountry|year|month|day|password|confirmPassword|
		##@externaldata@./src/test/resources/datadriven/BD_Demo.xlsx@BD
   |Jhon   |Pineda   |Calle 100   |jpinedav@choucairtesting.com   |3101232323   |Male   |Cricket   |Movies   |Hockey   |English   |Spanish   |Malay   |CSS   |Canada   |United States of America   |1985   |December   |23   |Abc123456789   |Abc123456789|
   |Jose   |Pino   |Calle 120   |jpino@choucairtesting.com   |2157887889   |FeMale   |Cricket   |Movies   |   |English   |Spanish   |Korean   |C++   |Algeria   |Japan   |1995   |November   |12   |Abc123456789   |Abc123456789|
   |Juliana   |Costa   |Calle 20   |jcosta@choucairtesting.com   |3213456778   |Male   |Cricket   |   |Hockey   |English   |Polish   |Spanish   |iOS   |Canada   |United States of America   |1945   |April   |14   |Abc123456789   |Abc123456789|
   |Carlo   |Posada   |Carrera 52   |cposada@choucairtesting.com   |3215589898   |Male   |   |Movies   |   |Spanish   |English   |   |Android   |Brazil   |Australia   |1998   |December   |18   |Cda12234566   |Cda12234566|
