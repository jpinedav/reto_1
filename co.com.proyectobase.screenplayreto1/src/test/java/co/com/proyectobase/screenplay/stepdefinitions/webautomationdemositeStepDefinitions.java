package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.InformacionUsuario;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Registrar;
import co.com.proyectobase.screenplay.tasks.Validar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class webautomationdemositeStepDefinitions {
	
	@Managed(driver="chrome")
	private WebDriver suNavegador;
	private Actor carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial() 
	{
		carlos.can(BrowseTheWeb.with(suNavegador));
	}
	
	@Given("^que Carlos quiere acceder a la pagina de Registro$")
	public void queCarlosQuiereAccederALaRegistro() throws Exception {
		carlos.wasAbleTo(Abrir.LaWebAutomationdemosSite());
	}

	@When("^el realiza el registro en la pagina$")
	public void elRealizaElRegistroEnLaPGina(List<InformacionUsuario > dato) throws Exception
	{
		carlos.attemptsTo(Registrar.Usuario(dato));
	}

	@Then("^el verifica que se carga la pantalla con texto (.*)$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow(String palabraEsperada) throws Exception{
		carlos.should(seeThat(Validar.IngresoSatisfactorio(), equalTo(palabraEsperada)));
	}
}
